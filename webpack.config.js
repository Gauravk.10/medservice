const path = require('path');
const postCSSPlugins = [
    require('postcss-import'),
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('autoprefixer')
]

module.exports = {
    entry : './app/assets/scripts/app.js',
    output : {
        filename : 'app.bundled.js',
        path : path.resolve(__dirname,'app')
    },
    devServer: {
        before : function(app,server){
            server._watch('./app/**/*.html')// this is so that when we change any html file, it automatically refreshes the page
        },
        
        contentBase : path.resolve(__dirname,'app'),
        port : 2000,
        hot : true ,
        //'hot module' feature allows injecting css and js refresh
                    //this automatically watches the files for updates
        host:'0.0.0.0',
//        open : true
    },
    mode : 'development',
//    watch : true,
    module : {
        rules : [
            {
                test : /\.css$/i,
                use : [
                        'style-loader',//style-loader will take all our css and write it into the style tag in the html document
                        'css-loader?url=false',// the webpack is parsing acc to js , so we have to specify that when a css file comes,it                            has to load the css loader to parse acc to css 
                                                //by default, css loader handles urls on its own we have to specifically mention false to  stop that
                        
                        {
                            loader : 'postcss-loader',
                            options : {
                                postcssOptions : {
                                    plugins : postCSSPlugins
                
                                }
                
                            }
                
                    }
                ]
            }
        ]
    }
    
}